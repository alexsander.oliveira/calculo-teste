﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softplan.Teste.Api;
using Softplan.Teste.Infra.Integracao;
using Softplan.Teste.Infra.Integracao.Interfaces;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace Softplan.Teste.Teste.Integracao
{
    [TestClass]
    public class CalculoJurosIntegracao
    {
        private HttpClient _client { get; set; }
        private TestServer _server { get; set; }

        private readonly Mock<IJurosService> _jurosServiceMock;

        public CalculoJurosIntegracao()
        {
            _jurosServiceMock = new Mock<IJurosService>();
        }

        private void IniciaServer()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseUrls("http://localhost:8285")
                .ConfigureTestServices(services => {
                    services.RemoveAll<IJurosService>();
                    services.TryAddScoped<IJurosService>(sp => _jurosServiceMock.Object);
                })
                .UseEnvironment("Test")
                .UseStartup<Startup>());

            _client = _server.CreateClient();

            _client.BaseAddress = new Uri("http://localhost:8285");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        [TestMethod]
        [TestCategory("Integracao")]
        public async Task CalculaJurosComSucesso()
        {
            //Arrange
            _jurosServiceMock.Setup(x => x.ObterTaxaJuros()).Returns(new ResultadoConsultaDto<double>() 
            { 
                Dado = 0.01,
                Sucesso = true
            });

            IniciaServer();

            var response = await _server
                .CreateRequest("calculajuros?valorinicial=100&meses=5")
                .GetAsync();
            //Act
            var resultado = await response.Content.ReadAsStringAsync();
            var calculoJson = JsonSerializer.Deserialize<CalculoResultJson>(resultado);

            //Assert
            Assert.IsTrue(calculoJson.success, "O resutlado da consultajuros com sucesso não retornou o resultado esperado");
            Assert.IsTrue((double) calculoJson.data == 105.10, "O resutlado da consultajuros com sucesso não retornou o resultado esperado");
        }

        [TestMethod]
        [TestCategory("Integracao")]
        public async Task CalculaJurosComErroEntidade()
        {
            _jurosServiceMock.Setup(x => x.ObterTaxaJuros()).Returns(new ResultadoConsultaDto<double>()
            {
                Dado = 0.01,
                Sucesso = true
            });

            IniciaServer();

            var response = await _server
                .CreateRequest("calculajuros?valorinicial=100&meses=-5")
                .GetAsync();

            var resultado = await response.Content.ReadAsStringAsync();

            var calculoJson = JsonSerializer.Deserialize<CalculoResultJson>(resultado);

            Assert.IsFalse(calculoJson.success, "O resutlado da consultajuros com erro na entidade não retornou o resultado esperado");
            Assert.IsTrue(calculoJson.errors.Any(), "O resutlado da consultajuros com erro na entidade não retornou o resultado esperado");
        }

        [TestMethod]
        [TestCategory("Integracao")]
        public async Task CalculaJurosComErroNaIntegracao()
        {
            _jurosServiceMock.Setup(x => x.ObterTaxaJuros()).Returns(new ResultadoConsultaDto<double>()
            {
                Sucesso = false,
                Mensagem = "Erro na integração"
            });

            IniciaServer();

            var response = await _server
                .CreateRequest("calculajuros?valorinicial=100&meses=5")
                .GetAsync();

            var resultado = await response.Content.ReadAsStringAsync();

            var calculoJson = JsonSerializer.Deserialize<CalculoResultJson>(resultado);

            Assert.IsFalse(calculoJson.success, "O resutlado da consultajuros com erro na integração não retornou o resultado esperado");
            Assert.IsTrue(calculoJson.errors.Any(), "O resutlado da consultajuros com erro na entidade não retornou o resultado esperado");
        }
    }
}
