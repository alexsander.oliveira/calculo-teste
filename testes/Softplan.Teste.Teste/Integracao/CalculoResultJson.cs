﻿using System.Collections.Generic;

namespace Softplan.Teste.Teste.Integracao
{
    public class CalculoResultJson
    {
        public bool success { get; set; }
        public decimal data { get; set; }
        public List<string> errors { get; set; }
    
    }
}
