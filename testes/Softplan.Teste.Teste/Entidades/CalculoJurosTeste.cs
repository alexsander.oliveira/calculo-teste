﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Softplan.Teste.Dominio.Entidades;

namespace Softplan.Teste.Teste.Entidades
{
    [TestClass]
    public class CalculoJurosTeste
    {
        [TestMethod]
        public void EntidadeCalculoJurosComValorInicialInvalido()
        {
            //Arr
            var calculoJuros = new CalculoJuros(-100, 100);
            
            //Assert
            Assert.IsFalse(calculoJuros.Validacao.IsValid, "A validação da entidade com Valor inicial inválido não retornou o resultado esperado!");
        }
        [TestMethod]
        public void EntidadeCalculoJurosComMesesInvalido()
        {
            //Arr
            var calculoJuros = new CalculoJuros(100, -120);

            //Assert
            Assert.IsFalse(calculoJuros.Validacao.IsValid, "A validação da entidade com meses inválido não retornou o resultado esperado!");
        }

        [TestMethod]
        public void EntidadeCalculoJurosValida()
        {
            //Arr
            var calculoJuros = new CalculoJuros(100, 120);

            //Assert
            Assert.IsTrue(calculoJuros.Validacao.IsValid, "A validação da entidade válida não retornou o resultado esperado!");
        }
    }
}
