﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softplan.Teste.Dominio.Entidades;
using Softplan.Teste.Dominio.Interfaces;
using Softplan.Teste.Dominio.Servicos;
using Softplan.Teste.Infra.Integracao;
using Softplan.Teste.Infra.Integracao.Interfaces;

namespace Softplan.Teste.Teste.Dominio
{
    [TestClass]
    public class CalculaJurosServiceTeste
    {
        private readonly Mock<INotificacaoService> notificacaoService;
        private readonly Mock<IJurosService> jurosService;
        public CalculaJurosServiceTeste()
        {
            notificacaoService = new Mock<INotificacaoService>();
            jurosService = new Mock<IJurosService>();
        }

        [TestMethod]
        public void CalculaJurosCompostosComEntidadeInvalida()
        {
            // Arrange
            var calculaJuros = new CalculoJuros(100, -52);

            var calculaJurosService = new CalculoJurosService(
                notificacaoService.Object,
                jurosService.Object);

            // Act
            var resultado = calculaJurosService.CalculaJurosCompostos(calculaJuros);

            // Assert
            Assert.IsTrue(resultado == 0, "O método CalculaJurosCompostos com erro de entidade inválida não retornou o resultado esperado");
        }

        [TestMethod]
        public void CalculaJurosCompostosComErroServicoJuros()
        {
            // Arrange
            var calculaJuros = new CalculoJuros(100, 5);
            var resultadoConsulta = new ResultadoConsultaDto<double>()
            {
                Sucesso = false,
                Mensagem = "Erro ao processar"
            };
            notificacaoService.Setup(x => x.PossuiNotificacao()).Returns(true);

            jurosService.Setup(x => x.ObterTaxaJuros()).Returns(resultadoConsulta);

            var calculaJurosService = new CalculoJurosService(
                notificacaoService.Object,
                jurosService.Object);

            // Act
            var resultado = calculaJurosService.CalculaJurosCompostos(calculaJuros);

            // Assert
            Assert.IsTrue(resultado == 0, "O método CalculaJurosCompostos com erro no serviço de juros não retornou o resultado esperado");
        }

        [TestMethod]
        public void CalculaJurosCompostosComSucesso()
        {
            // Arrange
            var calculaJuros = new CalculoJuros(100, 5);
            var resultadoConsulta = new ResultadoConsultaDto<double>()
            {
                Sucesso = true,
                Dado = 0.01
            };

            jurosService.Setup(x => x.ObterTaxaJuros()).Returns(resultadoConsulta);

            var calculaJurosService = new CalculoJurosService(
                notificacaoService.Object,
                jurosService.Object);

            // Act
            var resultado = calculaJurosService.CalculaJurosCompostos(calculaJuros);

            // Assert
            Assert.IsTrue((double) resultado == 105.10, "O método CalculaJurosCompostos com sucesso não retornou o resultado esperado");
        }
    }
}
