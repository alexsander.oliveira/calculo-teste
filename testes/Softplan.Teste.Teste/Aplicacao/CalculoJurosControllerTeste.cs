﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softplan.Teste.Api.Controllers;
using Softplan.Teste.Dominio.Entidades;
using Softplan.Teste.Dominio.Interfaces;
using System.Collections.Generic;

namespace Softplan.Teste.Teste.Aplicacao
{
    [TestClass]
    public class CalculoJurosControllerTeste
    {
        private readonly Mock<INotificacaoService> _notificacaoService;
        private readonly Mock<ICalculoJurosService> _calculoJurosService;
        public CalculoJurosControllerTeste()
        {
            _notificacaoService = new Mock<INotificacaoService>();
            _calculoJurosService = new Mock<ICalculoJurosService>();
        }

        [TestMethod]
        public void CalcularJurosServiceComErro()
        {
            //Arrange
            var notificacoes = new List<Notificacao>()
            {
                new Notificacao("Erro")
            };
            _calculoJurosService.Setup(x => x.CalculaJurosCompostos(It.IsAny<CalculoJuros>())).Returns(0);
            _notificacaoService.Setup(x => x.PossuiNotificacao()).Returns(true);
            _notificacaoService.Setup(x => x.ObterNotificacoes()).Returns(notificacoes);

            var calculoJurosController = new CalculoJurosController( _notificacaoService.Object, _calculoJurosService.Object);

            //Act
            var resultado = calculoJurosController.CalculaJuros(100, 5);
            var result = (resultado as BadRequestObjectResult);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(400, result.StatusCode, "O método CalculoJurosController.CalculaJuros com erro não retornou o resultado esperado");
        }

        [TestMethod]
        public void CalcularJurosServiceComSucesso()
        {
            //Arrange
            _calculoJurosService.Setup(x => x.CalculaJurosCompostos(It.IsAny<CalculoJuros>())).Returns(10);
            var calculoJurosController = new CalculoJurosController(_notificacaoService.Object, _calculoJurosService.Object);

            //Act
            var resultado = calculoJurosController.CalculaJuros(100, 5);
            var result = (resultado as OkObjectResult);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode, "O método CalculoJurosController.CalculaJuros com sucesso não retornou o resultado esperado");
        }
    }
}
