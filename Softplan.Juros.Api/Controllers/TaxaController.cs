﻿using Microsoft.AspNetCore.Mvc;

namespace Softplan.Juros.Api.Controllers
{
    public class TaxaController : ControllerBase
    {
        [HttpGet]
        [Route("taxaJuros")]
        public double ObterTaxaJuros()
        {
            return 0.01;
        }
    }
}
