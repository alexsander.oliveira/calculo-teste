﻿using System;

namespace Softplan.Teste.Dominio.Entidades
{
    public class Notificacao
    {
        public Notificacao(string mensagem)
        {
            Id = Guid.NewGuid();
            Mensagem = mensagem;
        }

        public Guid Id { get; private set; }
        public string Mensagem { get; private set; }
    }
}
