﻿
using FluentValidation;
using FluentValidation.Results;

namespace Softplan.Teste.Dominio.Entidades
{
    public class CalculoJuros : AbstractValidator<CalculoJuros>
    {
        public CalculoJuros(decimal valorInicial, int meses)
        {
            ValorInicial = valorInicial;
            Meses = meses;
            Validacao = new ValidationResult();
            Validar();
        }

        public decimal ValorInicial { get; private set; }
        public int Meses { get; private set; }
        public ValidationResult Validacao { get; protected set; }

        public void Validar()
        {
            ValidarValorInicial();
            ValidarMeses();
            Validacao = Validate(this);
        }

        public void ValidarValorInicial()
        {
            RuleFor(x => x.ValorInicial)
                .Must(x => x > 0)
                .WithMessage("O valor inicial deve ser maior que zero");
        }

        public void ValidarMeses()
        {
            RuleFor(x => x.Meses)
                .Must(x => x > 0)
                .WithMessage("A quantidade de meses deve ser maior que zero");
        }

    }
}
