﻿using Softplan.Teste.Dominio.Entidades;
using Softplan.Teste.Dominio.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Softplan.Teste.Dominio.Servicos
{
    public class NotificacaoService : INotificacaoService
    {
        private readonly List<Notificacao> _notificacoes;

        public NotificacaoService()
        {
            _notificacoes = new List<Notificacao>();
        }

        public List<Notificacao> ObterNotificacoes()
        {
            return _notificacoes;
        }

        public bool PossuiNotificacao()
        {
            return _notificacoes.Any();
        }

        public void AdicionarNotificacao(Notificacao notificacao)
        {
            _notificacoes.Add(notificacao);
        }
    }
}
