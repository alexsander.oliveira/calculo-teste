﻿using FluentValidation.Results;
using Softplan.Teste.Dominio.Entidades;
using Softplan.Teste.Dominio.Interfaces;
using Softplan.Teste.Infra.Integracao.Interfaces;
using System;

namespace Softplan.Teste.Dominio.Servicos
{
    public class CalculoJurosService : ICalculoJurosService
    {
        private readonly INotificacaoService _notificacaoService;
        private readonly IJurosService _jurosService;
        public CalculoJurosService(INotificacaoService notificacaoService,
            IJurosService jurosService)
        {
            _jurosService = jurosService;
            _notificacaoService = notificacaoService;
        }

        public decimal CalculaJurosCompostos(CalculoJuros calculo)
        {
            if (!calculo.Validacao.IsValid)
            {
                AdicionarNotificacoesDaEntidade(calculo.Validacao);
                return 0;
            }

            var resultado = _jurosService.ObterTaxaJuros();

            if (!resultado.Sucesso)
                _notificacaoService.AdicionarNotificacao(new Notificacao(resultado.Mensagem));

            if (_notificacaoService.PossuiNotificacao())
                return 0;

            var jurosTotais = Math.Pow((1 + resultado.Dado), calculo.Meses);

            var resultadoCalculo = (double) calculo.ValorInicial * jurosTotais;

            return (decimal) Math.Round(resultadoCalculo, 2);
        }

        private void AdicionarNotificacoesDaEntidade(ValidationResult validationResult)
        {
            foreach (var erro in validationResult.Errors)
               _notificacaoService.AdicionarNotificacao(new Notificacao(erro.ErrorMessage));
        }
    }
}
