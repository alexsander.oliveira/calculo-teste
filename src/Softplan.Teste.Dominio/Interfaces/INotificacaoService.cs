﻿using Softplan.Teste.Dominio.Entidades;
using System.Collections.Generic;

namespace Softplan.Teste.Dominio.Interfaces
{
    public interface INotificacaoService
    {
        List<Notificacao> ObterNotificacoes();
        bool PossuiNotificacao();
        void AdicionarNotificacao(Notificacao notificacao);
    }
}
