﻿using Softplan.Teste.Dominio.Entidades;

namespace Softplan.Teste.Dominio.Interfaces
{
    public interface ICalculoJurosService
    {
        decimal CalculaJurosCompostos(CalculoJuros calculo);
    }
}
