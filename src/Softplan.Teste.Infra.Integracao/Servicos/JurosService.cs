﻿using Microsoft.Extensions.Configuration;
using Softplan.Teste.Infra.Integracao.Interfaces;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Text.Json;

namespace Softplan.Teste.Infra.Integracao.Servicos
{
    public class JurosService : IJurosService
    {
        private readonly Uri _apiUrl;

        public JurosService(IConfiguration configuration)
        {
            var url = configuration.GetSection("ApiJurosUrl").Value;
            _apiUrl = new Uri(url);
        }

        public ResultadoConsultaDto<double> ObterTaxaJuros()
        {
            try
            {
                return CallApi();
            }
            catch
            {
                return new ResultadoConsultaDto<double>()
                {
                    Sucesso = false,
                    Mensagem = "Ocorreu um erro ao comunicar com a Api de juros"
                };
            }
        }

        private ResultadoConsultaDto<double> CallApi()
        {
            using (HttpClientHandler clientHandler = new HttpClientHandler())
            {
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                {
                    if (Development())
                        return true;

                    return sslPolicyErrors == SslPolicyErrors.None;
                };

                using (HttpClient client = new HttpClient(clientHandler))
                {
                    client.BaseAddress = _apiUrl;
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = client.GetAsync("taxajuros").Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        return new ResultadoConsultaDto<double>()
                        {
                            Sucesso = false,
                            Mensagem = "Ocorreu um erro ao comunicar com a Api de juros"
                        };
                    }

                    var result = response.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrEmpty(result))
                    {
                        var taxaJuros = JsonSerializer.Deserialize<double>(result);

                        return new ResultadoConsultaDto<double>()
                        {
                            Sucesso = true,
                            Dado = taxaJuros
                        };
                    }
                    else
                    {
                        return new ResultadoConsultaDto<double>()
                        {
                            Sucesso = false,
                            Mensagem = "Ocorreu um erro na Api de juros não retornou nenhum registro"
                        };
                    }
                }
            }
        }

        private static bool Development()
        {
            bool dev = false;
            #if DEBUG
                dev = true;
            #endif
            return dev;
        }
    }
}
