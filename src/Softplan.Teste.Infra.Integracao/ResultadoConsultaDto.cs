﻿namespace Softplan.Teste.Infra.Integracao
{
    public class ResultadoConsultaDto<T> 
    {
        public T Dado { get; set; }
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
    }
}
