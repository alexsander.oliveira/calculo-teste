﻿namespace Softplan.Teste.Infra.Integracao.Interfaces
{
    public interface IJurosService
    {
        ResultadoConsultaDto<double> ObterTaxaJuros();
    }
}
