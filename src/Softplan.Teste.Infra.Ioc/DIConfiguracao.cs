﻿using Microsoft.Extensions.DependencyInjection;
using Softplan.Teste.Dominio.Interfaces;
using Softplan.Teste.Dominio.Servicos;
using Softplan.Teste.Infra.Integracao.Interfaces;
using Softplan.Teste.Infra.Integracao.Servicos;

namespace Softplan.Teste.Infra.Ioc
{
    public static class DIConfiguracao
    {
        public static IServiceCollection AdicionaInversaoDeControle(this IServiceCollection servicos)
        {
            //Serviços de domínio
            servicos.AddScoped<ICalculoJurosService, CalculoJurosService>();
            servicos.AddScoped<INotificacaoService, NotificacaoService>();

            //Serviços de integração
            servicos.AddScoped<IJurosService, JurosService>();

            return servicos;
        }
    }
}
