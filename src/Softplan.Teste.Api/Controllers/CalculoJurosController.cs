﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Softplan.Teste.Dominio.Entidades;
using Softplan.Teste.Dominio.Interfaces;
using System.Linq;

namespace Softplan.Teste.Api.Controllers
{
    public class CalculoJurosController : Controller
    {
        private readonly INotificacaoService _notificacaoService;
        private readonly ICalculoJurosService _calculoJurosService;
        private readonly IConfiguration _configuration;
        public CalculoJurosController(
            INotificacaoService notificacaoService, 
            ICalculoJurosService calculoJurosService,
            IConfiguration configuration)
        {
            _notificacaoService = notificacaoService;
            _calculoJurosService = calculoJurosService;
            _configuration = configuration;
        }

        [HttpGet]
        [Route("calculajuros")]
        public IActionResult CalculaJuros(decimal valorInicial, int meses)
        {
            var resultado = _calculoJurosService.CalculaJurosCompostos(
                new CalculoJuros(valorInicial, meses)
                );

            return Response(resultado);
        }

        [HttpGet]
        [Route("showmethecode")]
        public IActionResult Showmethecode()
        {
            var resultado = _configuration.GetSection("GitUrl").Value;

            return Response(resultado);
        }

        private new IActionResult Response(object result = null)
        {
            if (!_notificacaoService.PossuiNotificacao())
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }

            return BadRequest(new
            {
                success = false,
                errors = _notificacaoService.ObterNotificacoes().Select(n => n.Mensagem)
            });
        }

    }
}
